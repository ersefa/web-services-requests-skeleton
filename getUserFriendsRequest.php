<?php
  include_once 'wsRequest.php';

  class GetUserFriendsRequest extends WsRequest {

    public function __construct($userToken) {
      parent::__construct($userToken); 
      
      if (!isset($userToken)) {
        throw new Exception('Missing required params in getUserFriendsRequest.');
      }

      $this->method = BaseRequest::POST;

      $this->addQuery('limite', '20');
      $this->addQuery('offset', '0');
      $this->addQuery('pagination', '1');
      $this->addQuery('servicio', 'lista_amigos');
      $this->addQuery('origen', 'json');
    }

    public function processResponse($response) {
      return $response;
    }

    public function getMethod() {
      return $this->method;
    }
  }

?>