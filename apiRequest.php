<?php
  include_once 'baseRequest.php';

  class ApiRequest extends BaseRequest {
    private $requestURL = "https://ptg-dev.bitnamiapp.com:8443/PTGServices/ptgapi/v1/clients/3/users";
    private $authToken = "Basic dXNlcjp1c2Vy";
    private $channel = "web";
    private $contentType = "application/json";
    private $acceptLanguage = "es";
    private $finalURL;

    public function __construct() {
      parent::__construct();

      $this->addHeaders('Accept-Language', $this->acceptLanguage);
      $this->addHeaders('Authorization', $this->authToken);
      $this->addHeaders('X-ptg-channel', $this->channel); 
      $this->addHeaders('Content-Type', $this->contentType); 
    }

    public function getURL() {
      $this->finalURL = $this->requestURL;
      $this->composeSegments();
      $this->composeQueries();
      return $this->finalURL;
    }

    private function composeSegments() {
      if (!empty($this->segments)) {
        while (list($key, $value) = each($this->segments)) {
          $this->finalURL .= "/" . $value;
        }
      }
    }

    private function composeQueries() {
      if (!empty($this->queries)) {
        $this->finalURL .= "?";
        while (list($key, $value) = each($this->queries)) {
          $this->finalURL .= $key . "=" . $value . "&";
        }
      }
    }
  }
?>