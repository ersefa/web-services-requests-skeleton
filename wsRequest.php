<?php
  include_once 'baseRequest.php';

  class WsRequest extends BaseRequest {
    private $requestURL = "http://www.ptg-dev.com/xml.php";
    private $userToken;
    private $finalURL;

    public function __construct($userToken) {
      parent::__construct();
      $this->userToken = $userToken;
      $this->addHeaders('X-ptg-user-token', $this->userToken);
    }

    public function getURL() {
      $this->finalURL = $this->requestURL;
      $this->composeSegments();
      $this->composeQueries();
      return $this->finalURL;
    }

    private function composeSegments() {
      if (!empty($this->segments)) {
        while (list($key, $value) = each($this->segments)) {
          $this->finalURL .= "/" . $value;
        }
      }
    }
    
    private function composeQueries() {
      if (!empty($this->queries)) {
        $this->finalURL .= "?";
        while (list($key, $value) = each($this->queries)) {
          $this->finalURL .= $key . "=" . $value . "&";
        }
      }
    }
  }
?>