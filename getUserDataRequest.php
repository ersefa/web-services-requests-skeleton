<?php
  include_once 'apiRequest.php';

  class GetUserDataRequest extends ApiRequest {
    private $userId;
    private $userToken;

    public function __construct($userId, $userToken) {
      parent::__construct();
      
      if (!isset($userId) || !isset($userToken)) {
        throw new Exception('Missing required params in userData.');
      }
  
      $this->userId = $userId;
      $this->userToken = $userToken;

      $this->addHeaders('X-ptg-user-token', $userToken); 
      $this->addSegment($userId); 
    }

    public function processResponse($response) {
      return $response->payload;
    }
  }

?>