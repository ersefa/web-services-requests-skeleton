<?php
class BaseRequest {
  const GET = 1;
  const POST = 2;

  private $clientToken = "1ea71323-7566-3638-2w60-563htr856ott";

  protected $headers;
  protected $payloads; 
  protected $segments;
  protected $queries;
  protected $method = BaseRequest::GET;


  public function __construct() {
    $this->addHeaders('X-ptg-client-token', $this->clientToken); 
  }

  public function addHeaders($key, $value) {
    $this->headers[$key] = $value;
  }

  public function addBodyPayload($key, $value) {
    $this->payloads[$key] = $value;
  }

  public function addSegment($segment) {
    $this->segments[] += $segment;
  }

  public function addQuery($key, $value) {
    $this->queries[$key] = $value;
  }

  public function getPayLoad() {
    if (!empty($this->payloads)) {
      return json_encode($this->payloads);
    } else {
      return false;
    }
  }

  public function getHeaders() {
    if (!empty($this->headers)) {
      return $this->headers;
    } else {
      return array();
    }  
  }

  public function getMethod() {
    return $this->method;
  }

  public function processResponse($response) {
    return $response;
  }
}