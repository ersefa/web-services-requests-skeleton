<?php
  include_once 'apiRequest.php';

  class LoginRequest extends ApiRequest {
    private $userEmail;
    private $userPwd;

    public function __construct($userEmail, $userPwd) {
      parent::__construct(); 
      
      if (!isset($userEmail) || !isset($userPwd)) {
        throw new Exception('Missing required params in Login.');
      }

      $this->method = BaseRequest::POST;
  
      $this->userEmail = $userEmail;
      $this->userPwd = $userPwd;

      $this->addHeaders('X-ptg-user-email', $userEmail); 
      $this->addHeaders('X-ptg-user-password', $userPwd); 

      $this->addQuery('action', 'accesstokens');
      $this->addQuery('type', 'custom');

      $this->addBodyPayload('udid', '');
      $this->addBodyPayload('alias_device', '');
      $this->addBodyPayload('regid', '');
    }

    public function processResponse($response) {
      return $response->payload;
    }

    public function getMethod() {
      return $this->method;
    }
  }

?>